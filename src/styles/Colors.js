export default colors = {
  primary   : '#4690D6',
  secondary : '#FFDD63',
  terciary : '#4C9EB1',
  danger    : '#f53d3d',
  light     : '#f7f7f7',
  medium    : '#b0bec5',
  dark      : '#222222',
  greyed    : '#cccccc',
  darkGreyed: '#777777',
  explicit  : '#f44336',
}
